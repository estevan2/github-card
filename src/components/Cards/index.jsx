import { Grid, Paper } from "@material-ui/core"
import { makeStyles } from "@material-ui/core/styles"

const useStyle = makeStyles((theme) => ({
    root: {
        padding: "5px",
    },
    cardRepo: {
        display: "flex",
        alignItems: "center",
        marginTop: "10px"
    },
    info: {
        padding: "10px"
    },
    image: {
        height: "100px",
        margin: "10px",
        borderRadius: "100px"
    },
    title: {
        textAlign: "start"
    },
    subtitle: {
        textAlign: "start",
        color: "grey"
    }
}))

const Cards = ({ repoList }) => {

    const classes = useStyle()

    return (
        <div className={classes.root}>
            <Grid container>
                {repoList.map((item) => (
                    <Grid xs="12">
                        <Paper className={classes.cardRepo}>
                            <img className={classes.image} src={item.owner.avatar_url} alt="imagem do repositório" />
                            <div className={classes.info}>
                                <h1 className={classes.title}>{item.full_name}</h1>
                                <p className={classes.subtitle}>{item.description}</p>
                            </div>
                        </Paper>
                    </Grid>
                ))}
            </Grid>
        </div>
    )
}

export default Cards