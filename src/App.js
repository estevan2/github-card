import "./App.css"
import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles"
import { Button, Grid } from "@material-ui/core"
import Cards from "./components/Cards"
import exemplo from "./components/exemplo"

const useStyle = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    textAlign: "center",
  },
  button: {
    textAlign: "center",
    backgroundColor: "#077bf7",
    color: "#FFF",
    height: "35px"
  },
  input: {
    height: "34px",
    border: "none",
    fontSize: "20px"
  },
  nav: {},
  search: {
    paddingTop: "10px",
    margin: "20px 0"
  }
}))

function App() {

  const [textInput, setTextInput] = useState()
  const [repoList, setRepoList] = useState([])
  const [input, setInput] = useState()
  const [errorMessage, setErrorMessage] = useState(false)
  const [click, setClick] = useState(false)

  useEffect(() => {
    if (click) {
      fetch(`https://api.github.com/repos/${textInput}`)
        .then((x) => x.json())
        .then((x) => {
          if (x.message === undefined) {
            setRepoList([...repoList, x])
            setErrorMessage(false)
          } else {
            throw Error(x.message)
          }
          return x
        })
        .catch(e => setErrorMessage(e.message))
    }
  }, [textInput])

  const classes = useStyle()

  const triggerTextInput = (input) => {
    setTextInput(input)
  }

  return (

    <div className={classes.root}>
      <Grid container>
        <Grid xs="12" className={classes.nav}>
          <div className={classes.search}>
            <input type="text" className={classes.input} onChange={(e => setInput(e.target.value))} />
            <Button className={classes.button} onClick={() => (triggerTextInput(input), setClick(true))}>Procurar</Button>
            {errorMessage && <p>{errorMessage}</p>}
          </div>
        </Grid>
        <Grid xs="12">
          <Cards repoList={repoList} />
        </Grid>
      </Grid>
    </div >
  );
}

export default App;
